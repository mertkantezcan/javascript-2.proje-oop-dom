
const filmForm = document.querySelector("#film-form")
const titleInput = document.querySelector("#title")
const directorInput = document.querySelector("#director")
const urlInput = document.querySelector("#url")
const cardbody2 = document.querySelectorAll(".card-body")[1];
const clearall = document.getElementById("clear-films");


//Fonksiyonları kullanmak için
const ui = new UI();
const storage = new Storage();

eventListenerAll();

function eventListenerAll() {
    filmForm.addEventListener("submit", addFilm);
    document.addEventListener("DOMContentLoaded", loadAllValuestoUI);
    cardbody2.addEventListener("click", deleteElement)
    clearall.addEventListener("click", clearAll)

}

function addFilm(e) {
    const filmName = titleInput.value;
    const directorName = directorInput.value;
    const url = urlInput.value;

    if (filmName === "" || directorName === "" || url === "") {

        ui.AlertMessage("danger", "Boş alan bırakmayınız.")

    }
    else {
        const newFilm = new film(filmName, directorName, url);
        ui.addFilmtoUI(newFilm);
        storage.addFilmtoStorage(newFilm);

        ui.AlertMessage("success", "Başarıyla eklendi.")
    }
    ui.clearInputArea(titleInput, directorInput, urlInput);
    e.preventDefault();
}


function loadAllValuestoUI() {
    let films = storage.localStorageControl();

    films.forEach(function (e) {
        ui.addFilmtoUI(e);
    });
}

function deleteElement(e) {
    if (e.target.id === "delete-film") {
        ui.deleteElementUI(e.target);
        storage.deleteElementStorage(e.target.parentElement.previousElementSibling.previousElementSibling.textContent);
        ui.AlertMessage("success", "Silme işlemi başarılı");
    }
}

function clearAll() {
    if (confirm("Silmek istediğinize emin misiniz?")) {
        ui.clearAllUI();
        storage.clearAllStorage();
        ui.AlertMessage("success", "Silme işlemi başarı ile gerçekleşti.")
    }

}