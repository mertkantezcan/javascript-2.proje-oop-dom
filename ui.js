//Arayüz işlemleri için ui kullanıyoruz.
class UI {

    //Arayüze elemanı eklemek
    addFilmtoUI(newFilm) {

        const filmList = document.querySelector("#films");

        filmList.innerHTML += `
    <tr>
    <td><img src="${newFilm.filmUrl}" class="img-fluid img-thumbnail"></td>
    <td>${newFilm.filmTitle}</td>
    <td>${newFilm.filmDirector}</td>
    <td><a href="#" id = "delete-film" class = "btn btn-danger">Filmi Sil</a></td>
    </tr> `
    }

    //Ekleme işleminden sonra inputları boşaltma
    clearInputArea(element1, element2, element3) {
        element1.value = "";
        element2.value = "";
        element3.value = "";
    }

    //Alert Message yazdırma kısmı
    AlertMessage(element, message) {

        const cardbody1 = document.querySelectorAll(".card-body")[0];
        const alert = document.createElement("div");

        alert.innerHTML = `<br><div class="alert alert-${element}" role="alert">
        ${message}
      </div>`

        cardbody1.appendChild(alert);

        setTimeout(function () {
            alert.remove();
        }, 1000);
    }
    //Arayüzden elemanı silmek
    deleteElementUI(element) {
        element.parentElement.parentElement.remove();
    }


    clearAllUI() {
        let filmlist = document.querySelector("#films");

        while (filmlist.firstElementChild != null) {
            filmlist.firstElementChild.remove();

        }

    }

}