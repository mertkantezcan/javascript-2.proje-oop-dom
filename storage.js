class Storage {
    //Storage ekleme
    addFilmtoStorage(newFilm) {

        let films = this.localStorageControl();

        films.push(newFilm);

        films = localStorage.setItem("films", JSON.stringify(films));
    }

    //Local Storage da var mı yok mu kontrol
    localStorageControl() {
        let films;

        if (localStorage.getItem("films") === null) {
            films = []
        }

        else {
            films = JSON.parse(localStorage.getItem("films"));
        }
        return films;
    }

    deleteElementStorage(element) {
        let films = this.localStorageControl();
        films.forEach(function (film, index) {
            if (film.filmTitle === element) {

                films.splice(index, 1); // o indeksi sil.

            }

        })
        localStorage.setItem("films", JSON.stringify(films));
    }

    clearAllStorage() {
        let films = this.localStorageControl;
        localStorage.removeItem("films");
    }
}